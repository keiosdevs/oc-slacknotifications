<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/30/16
 * Time: 8:38 PM
 */

namespace Keios\SlackNotifications\Console;

use Illuminate\Console\Command;
use Keios\Monitor\Classes\CheckProcess;
use Keios\Monitor\Classes\Logger;
use Keios\Monitor\Classes\SshRunner;
use Keios\Monitor\Models\Process;
use Keios\SlackNotifications\Classes\SlackMessageSender;
use Maknz\Slack\Client;
use Maknz\Slack\Message;
use Symfony\Component\Console\Input\InputArgument;
use SSH;

/**
 * Class SlackNotifications
 *
 * @package Keios\SlackNotifications\Console
 */
class SendMessage extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'slack:send {message}';

    /**
     * The console command description.
     */
    protected $description = 'Send message to configured slack';


    /**
     * Execute the console command.
     *
     * @throws \ApplicationException
     * @throws \InvalidArgumentException
     */
    public function fire()
    {
        $sender = new SlackMessageSender();
        $txt = $this->argument('message');
        $sender->send($txt);
    }


    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [
            [
                'message',
                InputArgument::OPTIONAL,
                'Message to send',
            ],
        ];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [];
    }
}