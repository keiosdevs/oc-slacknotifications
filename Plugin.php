<?php namespace Keios\SlackNotifications;

use Backend;
use System\Classes\PluginBase;

/**
 * SlackNotifications Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'keios.slacknotifications::lang.plugin.name',
            'description' => 'keios.slacknotifications::lang.plugin.description',
            'author'      => 'Keios',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register('Keios\SlackNotifications\Classes\SlackServiceProvider');
        $this->bindCommands();
        $this->commands(
            [
                'slack:send',
            ]
        );
    }

    /**
     * Bind commands aliases
     */
    protected function bindCommands()
    {
        $this->app->bind(
            'slack:send',
            'Keios\SlackNotifications\Console\SendMessage'
        );
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'keios.slacknotifications.manage_settings' => [
                'tab'   => 'keios.slacknotifications::lang.plugin.name',
                'label' => 'keios.slacknotifications::lang.permissions.some_permission',
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerSettings()
    {
        return [
            'config' => [
                'label'       => 'Slack Notifications',
                'icon'        => 'icon-slack',
                'description' => 'Configure Slack Integration',
                'class'       => 'Keios\SlackNotifications\Models\Settings',
                'permissions' => ['keios.slacknotifications.manage_settings'],
            ],
        ];
    }
}
