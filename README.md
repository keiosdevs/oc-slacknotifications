## SlackNotifications

This is a simple plugin for OctoberCMS, that enables you to easily send messages to Slack from OctoberCMS plugins.

It uses [maknz/slack](https://github.com/maknz/slack) library to integrate with Slack.

### Installation

- Clone to october/plugins/keios/slacknotifications
- Issue *composer update* from the project root


### Configuration

- Create [Incoming WebHook](https://my.slack.com/services/new/incoming-webhook) in your Slack
- Go to Backend -> Settings -> Slack Notifications
- Paste your WebHook URL
- Provide name under which messages will appear
- Optionally provide emoji (eg ```:ghost:```) to give your bot an avatar.

### Usage

Command line:

```
php artisan slack:send "some message"
```

Simple, from code:

```
$slackSender = new \Keios\SlackNotifications\Classes\SlackMessageSender;
$slackSender->send('some message');
```

With ServiceProvider, from code:

```
/** @var \Maknz\Slack\Message $slackMessage */
$slackMessage = \App::make('slack_message');

$slackMessage->setText('some message');
$slackMessage->... // do additional modifications here
$slackMessage->send();
```