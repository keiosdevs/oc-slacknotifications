<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/2/17
 * Time: 4:49 PM
 */

namespace Keios\SlackNotifications\Classes;

use Illuminate\Support\ServiceProvider;
use Keios\SlackNotifications\Models\Settings;
use Maknz\Slack\Client;
use Maknz\Slack\Message;

/**
 * Class SlackServiceProvider
 *
 * @package Keios\SlackNotifications\Classes
 */
class SlackServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $settings = Settings::instance();
        $webhookUrl = $settings->get('webhook_url');
        $username = $settings->get('username');
        $channel = $settings->get('channel');
        $linkNames = $settings->get('link_names');
        $emoji = $settings->get('emoji');
        $slackSettings = [];
        if ($username) {
            $slackSettings['username'] = $username;
        }
        if ($channel) {
            $slackSettings['channel'] = $channel;
        }
        if ($linkNames) {
            $slackSettings['link_names'] = $linkNames;
        }

        $slack = new Client($webhookUrl, $slackSettings);
        if ($emoji) {
            $slack->setDefaultIcon($emoji);
        }
        $message = new Message($slack);
        $message->setUsername($username);
        $message->setIcon($emoji);
        $this->app['slack_message'] = $message;
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['slack_message'];
    }

}