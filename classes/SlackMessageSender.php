<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/2/17
 * Time: 5:14 PM
 */

namespace Keios\SlackNotifications\Classes;

use Maknz\Slack\Message;

/**
 * Class SlackMessageSender
 *
 * @package Keios\SlackNotifications\Classes
 */
class SlackMessageSender
{
    /**
     * @param $text
     */
    public function send($text, $customChannel = null)
    {
        /** @var Message $slackMessage */
        $slackMessage = \App::make('slack_message');
        if($customChannel) {
            $slackMessage->setChannel($customChannel);
        }
        $slackMessage->setText($text);
        $slackMessage->send();
    }
}
